const fs = require('fs')
const path = require('path')
const inquirer = require('inquirer')

const USERNAME = process.env.USERNAME || process.env.NAME
const HOME = process.env.USERPROFILE || process.env.HOME

const [DIRECTORY, SERVER] = path.dirname(process.cwd()).split(path.sep).reverse()
inquirer.prompt([
  {
    name: 'username',
    default: USERNAME
  },
  {
    name: 'agency',
    default: 'coredna'
  },
  {
    name: 'repo',
    default: DIRECTORY
  },
  {
    name: 'server',
    default: SERVER
  },
  {
    type: 'list',
    name: 'environment',
    default: answers => answers.agency === 'coredna' ? `westwood` : 'agency',
    when: answers => answers.agency === 'coredna',
    choices: [
      'westwood',
      'boston',
      'germany',
      'corednalabs',
      'other'
    ]
  },
  {
    type: 'list',
    name: 'remotePath',
    default: ({environment, server, repo, username}) => {
      switch (environment) {
        case 'boston':
          return `/mnt/bwired-web/sites/${server}/${repo}`
        case 'germany':
          return `/mnt/bwired-web/sites/${server}/${repo}`
        case 'corednalabs':
          return `/var/virtual/sites/${server}/${repo}`
      }
      return `/var/development/${username}/sites/${server}/${repo}`
    },
    choices: ({server, repo, username}) => [
      `/var/development/${username}/sites/${server}/${repo}`,
      `/var/virtual/sites/${server}/${repo}`,
      `/mnt/bwired-web/sites/${server}/${repo}`,
      `/sites/${server}/${repo}`,
      `other`
    ]
  },
  {
    name: 'key',
    default: `${HOME.replace(/\\/g, '/')}/.ssh/id_rsa`,
    validate: key => fs.existsSync(key) || `${key} does not exist!`
  },
  {
    type: 'list',
    name: 'extension',
    choices: [
      'corewebdna.net',
      'corewebdna.com',
      'filesmania.de',
      'corednalabs.com',
      'other'
    ],
    default: ({environment}) => {
      switch (environment) {
        case 'boston':
        case 'agency':
          return 'corewebdna.com'
        case 'germany':
          return 'filesmania.de'
        case 'corednalabs':
          return 'corednalabs.com'
        case 'westwood':
        default:
          return 'corewebdna.net'
      }
    }
  },
  {
    name: 'extension',
    when: (answers) => answers.extension === 'other'
  },
  {
    name: 'host',
    default: ({environment, extension, username, repo}) => {
      switch (environment) {
        case 'corednalabs':
        case 'boston':
          return `${username}.dxp1.dev.coredna.com`
        case 'agency':
          return `${repo}-${agency}-${username}.${extension}`
      }
      return '192.168.10.27'
    }
  },
  {
    type: 'input',
    name: 'siteUrl',
    default: ({repo, extension, username} = {}) =>
      `https://${repo}-${username}.${extension}`
  },
]).then(({
  host,
  key,
  remotePath,
  repo,
  server,
  siteUrl,
  username,
}) => {

  const defaultConfig = {
    root: '../',
    paths: {
      src: {
        javascript: `javascripts/src`,
        stylesheets: `stylesheets/src`,
      },
      dest: {
        javascript: `javascripts/dist`,
        stylesheets: `stylesheets/dist`,
      },
      remote: {
        javascript: `javascripts/dist`,
        stylesheets: `stylesheets/dist`,
      }
    },
    javascripts: {
      bundles: {
        all: {
          files: [
            'javascripts/src/**/*.js',
          ]
        },
        libs: {
          es6: false,
          files: [
            "jquery/dist/jquery.js",
          ]
        },
        checkout: {
          files: [
            'modules/checkout/javascripts/**/*.js'
          ]
        }
      }
    },
    stylesheets: {
      bundles: {
        all: {
          scss: true,
          files: [
            'stylesheets/src/all.scss',
          ]
        },
        checkout: {
          scss: true,
          files: [
            'modules/checkout/stylesheets/checkout.scss'
          ]
        }
      }
    }
  }

  const userConfig = {
    development: true,
    sourcemaps: true,
    deploy: true,
    sftp: {
      host,
      keyLocation: key,
      remotePath,
      username,
    },
    siteUrl
  }
  fs.writeFileSync('../config.user.json', JSON.stringify(userConfig, null, 2), 'utf-8')
  if (!fs.existsSync('../config.site.json')) {
    fs.writeFileSync('../config.site.json', JSON.stringify(defaultConfig, null, 2), 'utf-8')
    if (fs.existsSync('../.gitignore')) {
      fs.appendFileSync('../.gitignore', '\n!config.site.json')
      fs.appendFileSync('../.gitignore', '\nconfig.user.json')
    }
  }
  process.exit()
})

