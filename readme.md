# Gulp 4 Build System

* [NPM Tasks](#NPM Tasks)
* [References](#References)
* [Update](#Update)
* [Config](#Config)
* [Install](#Install)
* [Gulp Tasks](#Gulp Tasks)
* [Migrate](#Migrate)

## NPM Tasks
Ensure your current directory is the `.build` folder
* `npm run dev` - watch for changes, deployment, minification as specified in `config.user.json`
* `npm run build` - build minified version of files. use deploy setting from `config.user.json`
* `npm run update` - update the build system using [git subtree](https://www.atlassian.com/git/tutorials/git-subtree)

## References
* [Bitbucket repository](https://bitbucket.org/bwiredintegration/gulp4/src/master/) - build system repository
* [npm](https://docs.npmjs.com/) - package manager
* [gulp](https://gulpjs.com/docs/en/api/concepts) - build system
* [gulp-sftp-up4](https://www.npmjs.com/package/gulp-sftp-up4) - sftp deployment
* [glob](https://www.npmjs.com/package/glob) - file name pattern plugin
* [babel](https://babeljs.io/docs/en/) - javascript transpilation
* [gulp-sass](https://www.npmjs.com/package/gulp-sass) - stylesheet transpilation
* [git subtree](https://www.atlassian.com/git/tutorials/git-subtree) - build system installation command

## Install
Install gulp4 using git subtree, this will give you the ability to update your subtree at any time in the future.
```bash
git subtree add --prefix .build git@bitbucket.org:bwiredintegration/gulp4.git master --squash
```

## Change
Change the name of the `prototype.package.json` to `package.json`, add any non build dependencies to this file

## Update
Through subtree you can update your build system overtime and techniques evolve, you will need to perform a merge if your branch has changes from the master.
Once you have updated you may need to manually merge `prototype.package.json` with your `package.json` file
```bash
git subtree pull --prefix .build git@bitbucket.org:bwiredintegration/gulp4.git master --squash
```
Or
```bash
npm run update
```

## Config
run `npm run config` for a configuration wizard to get you set up

### Config files 
This build system aims to have your build be configurable via two config files, one personal one containing your private information and settings `config.user.json`, and one for site-wide settings to be shared between all developers `config.site.json`

If you need to customize `config.site.json` for your environment you can overwrite anything in it in your own `config.user.json` file, so you should **__never put custom settings or information in `config.site.json`__**

#### `config.site.json`
The site config has been separated into `paths`, `javascripts` and `stylesheets`.

`paths` is used to tell the build system about your environment to it can watch, build & deploy your files.

| prop | type | default | description |
|--- |--- |--- |--- |
| `paths.src.javascripts` | `(glob)` | `"javascripts/src/**/*.js"` | javascript root, used for watching |
| `paths.src.stylesheets` | `(glob)` | `"stylesheets/src/**/*.scss"` | stylesheet root, used for watching |
| `paths.dest.javascripts` | `(glob)` | `"javascripts/dist/**/*.scss"` | javascript compilation destination |
| `paths.dest.stylesheets` | `(glob)` | `"stylesheets/dist/**/*.scss"` | stylesheet compilation destination |
| `paths.remote.javascripts` | `(glob)` | `"javascripts/dist"` | javascripts deployment folder |
| `paths.remote.stylesheets` | `(glob)` | `"stylesheets/dist"` | stylesheets deployment folder |


#### Config - bundles
bundles have a particular shape that you will need to follow to have your files compiled by gulp
```json
{
  "bundles": {
    "bundleName": {
      "concat": true,
      "es6": true,
      "files": [
        "javascripts/src/absolute-file-name.js",
        "javascripts/src/glob/**/*.js",
        "node-package/source/browser.js"
      ]
    },
    "//": "...other bundles"
  }
}
```
| prop | type | default | description |
|--- |--- |--- |--- |
| `bundleName` | `(string)` | NA | Name of your bundle |
| `concat` | `(boolean, string)` | `true` | concat can be either a boolean or a filename to concat, if a `true` is provided the name of the *bundleName* will be used |
| `es6` | `(boolean)` | `true` |  Use babel to transpile es6 to es5 (javascripts only) |
| `scss` | `(boolean)` | `true` |  Use scss to transpile scss to css (stylesheets only) |
| `files` | `(Array<Glob, string>)` | `[]` | Array of [globs](https://www.npmjs.com/package/glob) / filenames / node packages to compile. With node packages be sure to use the browser friendly version of the package (files are relative to the repository root, not the `.build` folder) |

#### `config.user.json`
The user config allows you to enter your specific information for your environment, it is added to the .gitignore if it's available when running `npm run config`.

| prop | type | default | description |
|--- |--- |--- |--- |
| `development` | `(boolean)` | `true` | Minify the output of your build |
| `sourcemaps` | `(boolean)` | `true` | Generate sourcemaps so you can view the code in the un-transpiled/minified code in browser |
| `deploy` | `(boolean)` | `true` | Use sftp to deploy your changes to the server on save |
| `debug` | `(boolean)` | `false` | print the names of the files for all the bundles |
| `sftp` | `(Object)` | See below | Sftp configuration using [gulp-sftp](https://www.npmjs.com/package/gulp-sftp-up4) |
| `sftp.host` | `(string)` | `192.168.10.27` | Westwood host |
| `sftp.key` | `(string)` | `path/to/your/id_rsa` | Path to your id_rsa if found when configuring |
| `sftp.remotePath` | `(string)` | `/remote/deployment/path` | Remote deployment path chosen during config |
| `sftp.username` | `(string)` | `null` | Username for deployment, chosen during config |
| `siteUrl` | `(string)` | `` | Your front end development url to proxy |

## Gulp Tasks
Gulp tasks will only run if you globally install gulp and gulp-cli to your system. this method is less future proof.
* `gulp` - rebuild files and run watch
* `gulp dev` - run gulp in development mode
* `gulp build` - rebuild files
* `gulp build:css` - rebuild css files
* `gulp build:js` - rebuild js files
* `gulp prod` - run gulp in production mode
* `gulp watch` - watch for js and css changes

## Migrate
Gulp 3 and Gulp 4 are not compatible. You will need to do some work to migrate across the old dependencies into the new build system.

1. If you have an existing gulp 3 install in a folder named `.build` rename it while we migrate eg. `.gulp3-build`

2. Run the build system [install script](#Install) through git subtree

3. Copy and non build system dependencies in the `.gulp3-build/package.json` into the new `.build/package.json` this is not as simple as leaving behind the dev dependencies in most cases, you will need to carefully copy them across.

4. Check the paths in the `config.site.json`. You will need to look at the existing tasks in gulp3 and the `.gulp3-build/config.json` if it's been used in your legacy site.

If you have the following `gulp.src` in `.gulp3-build/gulpfile.js` you would need to migrate it across to the new `config.site.json`, ensure you check for all javascript and stylesheet bundles.

> remember that the paths in the new `config.site.json` are resolved from the root of your website, you may need to adjust the paths.

`.gulp3-build/gulpfile.js`
```js
gulp.src([
  '.build/javascripts/src/**/*.js', 
  '.build/javascripts/src/some-file.js'
])
.pipe(concat('main.js'))
```
`config.site.json`
```json
{
  "bundles": {
    "main": {
      "concat": true,
      "es6": true,
      "files": [
        "javascripts/src/**/*.js", 
        "javascripts/src/some-file.js"
      ]
    }
  }
}
```

## Troubleshooting
* If you cannot install using git subtree, you likely don't have an [ssh public key](https://bitbucket.org/account/settings/ssh-keys/) attached to your [bitbucket account](https://bitbucket.org/account/settings/).
* If you have missing functionality you likely have not migrated the old packages from the old `package.json` to the new `package.json` file.
