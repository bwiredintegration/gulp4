const fs = require('fs')
const path = require('path')

const { src, dest, series, parallel, watch: gulpWatch } = require('gulp')

const concat = require('gulp-concat')
const _if = require('gulp-if')
const babel = require('gulp-babel')
const browserSync = require('browser-sync').create()
const sass = require('gulp-sass')
const deploy = require('gulp-sftp-up4')
const uglify = require('gulp-uglify')
const plumber = require('gulp-plumber')
const postcss = require('gulp-postcss')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const gulpSourcemaps = require('gulp-sourcemaps')
const chalk = require('chalk')

// used to set the current working directory of src() and dest() to the site root folder

if (!fs.existsSync('../config.user.json')) {
  console.log("")
  console.log(`  ${chalk.red('No config found')}, Please run ${chalk.cyan(' npm run config ')}`)
  process.exit()
}

const defaultConfig = JSON.parse(fs.readFileSync('../config.site.json', 'utf-8'))
const userConfig = JSON.parse(fs.readFileSync('../config.user.json', 'utf-8'))

let {
  root = '../',
  deploy: useDeploy = true,
  debug: useDebug = false,
  development: isDev,
  javascripts,
  paths,
  sftp,
  siteUrl: proxy,
  sourcemaps: useSourcemaps,
  stylesheets,
} = { ...defaultConfig, ...userConfig }

// this is intentionally a let variable

const cwd = path.resolve(__dirname, root)
const trace = tag => x => useDebug ? (console.log(chalk.magenta(tag), x), x) : x
const resolvePathToRoot = path => root + path
const concatFiles = bundles => Object.keys(bundles)
  .reduce((acc, name) => acc.concat(bundles[name].files))

if (sftp.key) sftp.key = fs.readFileSync(sftp.key, 'utf8')


/**
 * this will go through the bundles and dynamically generate tasks for each bundle in your config
 * @param runner  gulp task runner (series, parallel)
 * @param compiler  compiler function (contains gulp tasks)
 * @param bundles  bundles from config.site.json
 * @returns {function(*=, *=): function(*): *}
 */
const generateBuild = runner => (compiler, bundles) => function generate(done) {
  const dynamicTasks = Object.keys(bundles).map(name =>
    compiler(name, bundles[name])
  )
  return runner(...dynamicTasks, function tasks(tasksDone) {
    done()
    tasksDone()
  })()
}
const generateBuildSeries = generateBuild(series)
const generateBuildParallel = generateBuild(parallel)


const compileJavascripts = (name, {
  files,
  concat: useConcat = true,
  es6: useEs6 = true
} = {}) => {
  function task(done) {
    src(files.map(resolveDependencies), { cwd })
      .pipe(_if(useSourcemaps, gulpSourcemaps.init()))
      .pipe(plumber(plumberizer))
      .pipe(_if(useEs6, babel({
        ignore: ['libs.js', '/node_modules/', '/bower_components/'],
        presets: [
          [require('@babel/preset-env'), { "modules": false }]
        ],
        plugins: [
          [require('@babel/plugin-proposal-decorators'), { 'legacy': true }],
          require('@babel/plugin-proposal-function-sent'),
          require('@babel/plugin-proposal-export-namespace-from'),
          require('@babel/plugin-proposal-numeric-separator'),
          require('@babel/plugin-proposal-throw-expressions'),
        ]
      })))
      .pipe(_if(isDev, uglify()))
      .pipe(_if(useConcat, concat(useConcat === true ? `${name}.js` : useConcat)))
      .pipe(_if(useSourcemaps, gulpSourcemaps.write()))
      .pipe(dest(paths.dest.javascript, { cwd }))
      .pipe(_if(useDeploy, deploy({
        ...sftp,
        remotePath: path.join(sftp.remotePath, paths.remote.javascript)
      })))
    done()
  }
  task.displayName = `javascript:${name}`
  return task
}


const compileStylesheets = (name, {
  files,
  concat: useConcat = true,
  scss: useScss = true
} = {}) => {
  function task(done) {
    src(files.map(resolveDependencies), { cwd })
      .pipe(_if(useSourcemaps, gulpSourcemaps.init()))
      .pipe(_if(useScss, sass({
        includePaths: [
          path.resolve(__dirname, './node_modules')
        ]
      }).on('error', sass.logError)))
      .pipe(postcss([
        require('precss'),
        autoprefixer(),
        cssnano()
      ]))
      .pipe(_if(useSourcemaps, gulpSourcemaps.write()))
      .pipe(dest(paths.dest.stylesheets, { cwd }))
      .pipe(_if(useDeploy, deploy({
        ...sftp,
        remotePath: path.join(sftp.remotePath, paths.remote.stylesheets)
      })))
      .pipe(browserSync.stream())
    done()
  }
  task.displayName = `stylesheet:${name}`
  return task
}


const sync = (done) => {
  browserSync.init({
    proxy,
    https: true,
    files: trace('files')([
      resolvePathToRoot(paths.dest.javascript) + '/**/*.js',
      resolvePathToRoot(paths.dest.stylesheets) + '/**/*.css'
    ]),
    serveStatic: [
      resolvePathToRoot(paths.dest.javascript) + '/**/*.js',
      resolvePathToRoot(paths.dest.stylesheets) + '/**/*.css'
    ],
    notify: true,
    open: false,
  })
  done()
}


const watch = (done) => {
  gulpWatch(resolvePathToRoot(paths.src.javascript), series(exports['build:javascripts']))
  gulpWatch(resolvePathToRoot(paths.src.stylesheets), series(exports['build:stylesheets']))
  gulpWatch(['templates/**/*.html', 'modules/**/*.html'].map(resolvePathToRoot)).on('change', browserSync.reload)
  done()
}


const environment = (env = 'development') => (done) => {
  if (env === 'development') {
    isDev = true
  } else if (env === 'production') {
    isDev = false
    useSourcemaps = false
    useDebug = false
  }
  done()
}


exports['build:js'] = exports['build:javascripts'] = generateBuildParallel(compileJavascripts, javascripts.bundles)
exports['build:css'] = exports['build:stylesheets'] = generateBuildParallel(compileStylesheets, stylesheets.bundles)


exports['default'] = series(
  watch,
  sync,
  parallel(
    exports['build:javascripts'],
    exports['build:stylesheets']
  )
)

exports['dev'] = exports['default']

exports['build'] = parallel(
  exports['build:javascripts'],
  exports['build:stylesheets']
)

exports['build:prod'] = series(
  environment('production'),
  parallel(
    exports['build:javascripts'],
    exports['build:stylesheets']
  )
)

exports['build:dev'] = series(
  environment('development'),
  parallel(
    exports['build:javascripts'],
    exports['build:stylesheets']
  )
)


exports['watch'] = series(watch)

function normalizePath(path) { return path.replace(/\\/g, '/') }
function resolveDependencies(dependency) {
  if (~dependency.indexOf('*')) {
    return trace('glob')(dependency)
  }
  if (fs.existsSync(path.resolve(__dirname, '../', dependency))) {
    return trace('exists')(normalizePath(path.resolve(__dirname, '../', dependency)))
  }
  try {
    return trace('resolved')(normalizePath(require.resolve(dependency)))
  } catch (e) {
  } finally {
  }
  return trace('else')(normalizePath(dependency))
}

function plumberizer(err) {
  console.log(err.toString());
  this.emit('end');
}
